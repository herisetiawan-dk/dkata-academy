const { cube, exp, sqr } = require('./SecondDay-exc1')

test('Exponent function', () => {
    expect(exp(7, -5)).toBe(0.000059499018266198606)
})

test('Testing counter area of square', () => {
    expect(sqr(-2)).toBe(4)
})

test('Testing counter area of cube', () => {
    expect(cube(-2)).toBe(-8)
})