//Creating function that can handle exponent of number
const exp = (a, b) => {
  return a ** b
}

//Creating how to count Square or cube
const count = function (type, b) {
  switch (type) {
    case 'sqr':
      return b ** 2;
    case 'cube':
      return b ** 3;
  }
}

//Try different way here
const sqr = function (a) {
  return a ** 2
}

const cube = function (a) {
  return a ** 3
}

//Thats is unefficient, you can try this this one
const sqr = function (a) {
  return exp(a, 2)
}

const cube = function (a) {
  return exp(a, 3)
}

//or this
const count = function (type, b) {
  switch (type) {
    case 'sqr':
      return exp(a, 2);
    case 'cube':
      return exp(a, 3);
  }
}

const sum = (a, b) => {
  return a + b
}
//Creating function to sum an array using .reduce()
const reducer = function (array, minlen) {
  return array.map(a => a.length).filter(a => a >= minlen).reduce(sum)
}

//Using JEST for test the code, just type it in terminal to install
npm i jest - g

// example code:
test('Testing reducer', () => {
  expect(math.reducer(cities, 7)).toBe(14)
})

//Counting customer in, out, and current
cust = 0
total = 0

custIn = () => { cust++; total++ }
custOut = () => { cust-- }
custGetCur = () => { return cust }
custGetTotal = () => { return total }