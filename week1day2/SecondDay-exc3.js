cust = 0
total = 0

const example = () => {
    custIn = () => { cust++; total++ }
    custOut = () => { cust-- }
    custGetCur = () => { return cust }
    custGetTotal = () => { return total }
}

const count = () => {
    let value = 0
    const incr = () => {
        value += 1
    }
    const decr = () => {
        value -= 1
    }
    const getVal = () => {
        return value
    }
    return { incr, decr, getVal }
}

module.exports = { count }