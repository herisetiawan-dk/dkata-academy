const Joi = require('@hapi/joi')
const handler = require('../../handler/petsHandler')
const valid = require('../../validator')


exports.pets = [
    {
        method: 'GET',
        path: '/api/pets',
        options: {
            validate: {
                query: {
                    sort: Joi.string().valid('id', 'name', 'breed','colour','age','next_checkup','vaccinations'),
                    limit: Joi.number().integer(),
                    offset: Joi.number().integer(),
                    filter: Joi.string().valid('all','sold','unsold'),
                }
            }
        },
        handler: handler.pets,
    }
]

exports.petsId = [
    {
        method: 'GET',
        path: '/api/pets/{id}',
        handler: handler.petsId,
        options: {
            validate: {
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.setPet = [
    {
        method: 'POST',
        path: '/api/pets',
        handler: handler.setPet,
        options: {
            validate: {
                payload: {
                    name: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                    breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                    colour: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                    age: Joi.number().integer().required(),
                    next_checkup: Joi.date(),
                    vaccinations: Joi.array()
                }
            }
        }
    }
]

exports.putPet = [
    {
        method: 'PUT',
        path: '/api/pets/{id}',
        handler: handler.putPet,
        options: {
            validate: {
                payload: {
                    next_checkup: Joi.date()
                },
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.delPet = [
    {
        method: 'DELETE',
        path: '/api/pets/{id}',
        handler: handler.delPet,
        options: {
            validate: {
                params: {
                    id: valid.num
                }
            }
        }
    }
]

exports.allPet = [
    {
        method: 'GET',
        path: '/api/pets/all',
        handler: handler.allPet
    }
]