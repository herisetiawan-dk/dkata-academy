const users = {
    heri: {
        username: 'heri',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'Heri Setiawan',
        id: '2133d32a'
    }
};

module.exports = users