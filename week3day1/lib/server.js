const Hapi = require('@hapi/hapi')
const api = require('../routes/api')
const log = require('../tools/log')
const users = require('./users')
const Bcrypt = require('bcrypt')




const server = Hapi.server({
    host: '0.0.0.0',
    port: 3000,
});
const validate = async (request, username, password, h) => {

    if (username === 'help') {
        return { response: h.redirect('https://hapijs.com/help') };     // custom response
    }

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

server.register(require('@hapi/basic'));
server.route(api);
server.auth.strategy('simple', 'basic', { validate });
server.auth.default('simple');

const start = async () => {
    await server.start();
    console.log('Server running at:', server.info.uri);
    server.events.on('response', (request) => {
        log(request);
    });
    return server
};

const init = async () => {
    await server.initialize();
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

exports.start = start
exports.init = init