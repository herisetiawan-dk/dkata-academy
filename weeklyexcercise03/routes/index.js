const { rootIndex } = require('./rootIndex')
const { getTask, postTask, putTask, getTaskById, removeTask } = require('./tasksRoute')

module.exports = [].concat(rootIndex, getTask, postTask, putTask, getTaskById, removeTask)