const handler = require('../handler')

const rootIndex = [
    {
        method: 'GET',
        path: '/',
        options: {
            description: 'Root path',
            notes: 'Greeting and welcoming users',
            tags: ['root']
        },
        handler: handler.rootIndex,
    }
]

module.exports = {
    rootIndex
}