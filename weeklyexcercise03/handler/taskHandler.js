const { Task } = require('../models/taskModel')

const idChecker = async (id) => {
    let petReq = await Task.find({ id: id }, { _id: 0 })
    return petReq[0] != undefined
}

const dateChecker = (date) => {
    let curDate = Date.parse(new Date().toISOString().slice(0,10))
    return Date.parse(date) >= curDate || date == undefined
}

const getTask = async (request, h) => {
    let { filter, sort, desc, date } = request.query
    filter = filter == undefined ? {} : { status: filter }
    // console.log(date.toString())
    date = date == undefined ? {} : { finishedAt: date }
    sort = sort ? sort : 'createdAt'
    desc = desc ? -1 : 1
    Object.assign(filter, date)
    return await Task.find(filter).sort({ [sort]: desc })
}

const getTaskById = async (request, h) => {
    if (!idChecker(request.params.id)) {
        return h.response({ message: 'Not Found' }).code(404)
    }
    return Task.find({ id: request.params.id })
}

const postTask = async (request, h) => {
    if (!dateChecker(request.payload.finishedAt)) {
        return h.response({ message: "You cant go to past time. So, just think about future" }).code(400)
    }
    try {
        let max = await Task.aggregate([{ $group: { _id: 'max', maxId: { $max: "$id" } } }])
        max = max[0] == undefined ? 0 : max[0]['maxId']
        request.payload.id = max + 1
        request.payload.createdAt = new Date()
        request.payload.updatedAt = new Date()
        request.payload.status = 'new'
        let newTask = new Task(request.payload)
        await newTask.save()
        h.response().code(201)
        return newTask
    } catch (err) {
        h.response({ message: err.message }).code(400)
    }
}

const putTask = async (request, h) => {
    let { params } = request
    let current = await Task.find({ id: params.id }, { _id: 0 })
    request.payload.status != undefined ? null : current[0].status == 'complete' ? null : request.payload.status = 'in-progress'
    request.payload.updatedAt = new Date()

    if (!idChecker(params.id)) {
        return h.response({ message: 'Not Found' }).code(404)
    }

    if (!dateChecker(request.payload.finishedAt)) {
        return h.response({ message: "You cant go to past time. So, just think about future" }).code(400)
    }

    if (request.payload.comments != undefined) {
        request.payload.comments = (current[0].comments).concat(
            {
                body: request.payload.comments,
                date: new Date()
            }
        )

    }

    await Task.findOneAndUpdate({ id: params.id }, request.payload)
    h.response().code(202)
    return Task.find({ id: params.id })
}

const removeTask = async (request, h) => {
    if (!idChecker(request.params.id)) {
        return h.response({ message: 'Not Found' }).code(404)
    }
    return await Task.deleteOne({ id: request.params.id })
}

module.exports = {
    getTask,
    getTaskById,
    postTask,
    putTask,
    removeTask
}
