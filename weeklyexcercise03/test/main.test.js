const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init, plugReg } = require('../lib/server')

plugReg()

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Try open Task API', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Try get Task by date', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks?date=2244-09-08T17:00:00.000Z'
        });
        expect((res.result[0].finishedAt).toString()).to.equal('Mon Sep 09 2244 00:00:00 GMT+0700 (Western Indonesia Time)');
    });

    it('Filter for just getting in-progress task', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks?filter=in-progress'
        })
        expect(res.result[0].status).to.equal('in-progress')
    })

    it('Sort all task by createdAt as descending', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks?sort=createdAt&desc=true'
        })
        expect(res.result.length > 0).to.equal(true)
    })

    it('Getting task by date with just complete status, sorted by id ascending', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks?date=2244-09-08T17:00:00.000Z&filter=complete&sort=id'
        })
        expect(res.result[0].status).to.equal('complete')
        expect((res.result[0].finishedAt).toString()).to.equal('Mon Sep 09 2244 00:00:00 GMT+0700 (Western Indonesia Time)')
    })

    it('If query is not vaalid, response must be 400', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks?date=2244-19-48T17:00:00.000Z&filter=nothing&sort=false'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('Getting task by id', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/tasks/2'
        })
        expect(res.result[0].description).to.equal('Just waiting world invated by alien')
    })

    it('What is the response if id is not found', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/task/947'
        })
        expect(res.statusCode).to.equal(404)
    })

    it('What is the response if id is not integer', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/task/two'
        })
        expect(res.statusCode).to.equal(404)
    })

    it('What is the response if id is a negative number', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/task/-94'
        })
        expect(res.statusCode).to.equal(404)
    })

    it('What is the response if id is have a comma', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/task/1.2'
        })
        expect(res.statusCode).to.equal(404)
    })

    it('Posting a new task', async() => {
        const res = await server.inject({
            method: 'post',
            url: '/tasks',
            payload: {
                title: "Vacation to black hole",
                description: "Doing a tour to see black hole closely",
                finishedAt: "2020-03-18"
            }
        })
        expect(res.result.title).to.equal("Vacation to black hole")
        expect(res.result.status).to.equal('new')
    })

    it('Posting a new task with past time as targer', async() => {
        const res = await server.inject({
            method: 'post',
            url: '/tasks',
            payload: {
                title: "Seeing Dinosauruses",
                description: "Go to stone age to see dinosauruses",
                finishedAt: "0001-1-1"
            }
        })
        expect(res.result.message).to.equal("You cant go to past time. So, just think about future")
    })

    it("Posting a new task without sending a payload", async() => {
        const res = await server.inject({
            method: 'post',
            url: '/tasks'
        })
        expect(res.result.message).to.equal('Invalid request payload input')
        expect(res.statusCode).to.equal(400)
    })

    it('Updating vaccination to black hole because cant trip to there in 2020', async() => {
        const resMax = await server.inject({
            method: 'get',
            url: '/tasks'
        })
        let maxId = Math.max(...(resMax.result).map(x => x.id))
        const res = await server.inject({
            method: 'put',
            url: `/tasks/${maxId}`,
            payload: {
                finishedAt: '2315-12-19',
                comments: 'revision the date'
            }
        })
        expect(res.result[0].status).to.equal('in-progress')
    })

    it('Updating a task without payload', async() => {
        const resMax = await server.inject({
            method: 'get',
            url: '/tasks'
        })
        let maxId = Math.max(...(resMax.result).map(x => x.id))
        const res = await server.inject({
            method: 'put',
            url: `/tasks/${maxId}`,
        })
        expect(res.result.message).to.equal('Invalid request payload input')
        expect(res.statusCode).to.equal(400)
    })

    it('Marking task as complete', async () => {
        const resMax = await server.inject({
            method: 'get',
            url: '/tasks'
        })
        let maxId = Math.max(...(resMax.result).map(x => x.id))
        const res = await server.inject({
            method: 'put',
            url: `/tasks/${maxId}`,
            payload: {
                status: 'complete'
            }
        })
        expect(res.result[0].status).to.equal('complete')
        
    })

    it('Deleting a post that we cheated before', async() => {
        const resMax = await server.inject({
            method: 'get',
            url: '/tasks'
        })
        let maxId = Math.max(...(resMax.result).map(x => x.id))
        const res = await server.inject({
            method: 'delete',
            url: `/tasks/${maxId}`
        })
        expect(res.result).to.equal({ n: 1, ok: 1, deletedCount: 1 })
    })
});