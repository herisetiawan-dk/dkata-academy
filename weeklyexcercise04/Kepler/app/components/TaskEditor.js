import React, { Component } from 'react'
import { View, FlatList, Text, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { style, mainColor, secColor } from '../styles'
import { updateTask, createTask, addComment, markAs } from '../actions'
import DateTimePicker from '@react-native-community/datetimepicker'
import renderComment from './CommentsList'

class TaskEditor extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            id: null,
            loading: true,
            title: '',
            desc: '',
            finishedAt: new Date().toISOString().slice(0,10),
            comments: [],
            newComment: '',
            status: '',
            datePicker: false
        }
    }

    componentDidMount() {
        const { task } = this.props.navigation.state.params
        if (task.status !== 'add') {
            this.setState({
                id: task.id,
                title: task.title,
                desc: task.description,
                finishedAt: (task.finishedAt).slice(0, 10),
                comments: task.comments,
                status: task.status
            })
        }
        this.setState({
            loading: false
        })

    }

    componentWillUnmount() {
        const { id, title, desc, finishedAt } = this.state
        if (title.length, desc.length > 0) {
            if (id != null) {
                this.props.updateTask(id, title, desc, finishedAt)
            } else {
                this.props.createTask(title, desc, finishedAt)
            }
        }
    }

    setDate = (event, date) => {
        date = date || this.state.date
        this.setState({
            datePicker: false,
            finishedAt: date.toISOString().slice(0, 10)
        })
    }

    addComment = () => {
        const { id, newComment, comments } = this.state
        if (newComment.length > 0) {
            this.props.addComment(id, newComment)
            this.setState({
                comments: comments.concat({
                    body: newComment,
                    date: new Date().toISOString()
                }),
                newComment: ''
            })
        }
    }

    markAs = () => {
        const { id, status } = this.state
        const onPressed = () => {
            switch (status) {
                case 'new':
                    this.props.markAs(id, 'in-progress')
                    this.setState({
                        status: 'in-progress'
                    })
                    break
                case 'in-progress':
                    this.props.markAs(id, 'complete')
                    this.setState({
                        status: 'complete'
                    })
                    break
                case 'complete':
                    ToastAndroid.show('Task already completed', ToastAndroid.SHORT);
            }
        }
        const statusTxt = () => {
            switch (status) {
                case 'new':
                    return "Mark as in-Progress"
                case 'in-progress':
                    return "Mark as Complete"
                case 'complete':
                    return "Task Completed"
            }
        }
        return { onPressed, statusTxt }
    }

    render() {
        return (
            <View style={[style.container, style.fullContainer, style.containerPadding]}>
                {this.state.datePicker &&
                    <DateTimePicker
                        value={Date.parse(this.state.finishedAt)}
                        mode='date'
                        display='default'
                        minimumDate={new Date()}
                        onChange={this.setDate}
                    />
                }
                <View style={[style.datePickerContainer, style.titleDateContainer]}>
                    <TextInput
                        style={[style.basicInput, style.titleInput]}
                        value={this.state.title}
                        onChangeText={(title) => this.setState({ title })}
                    />
                    <View style={[style.basicInput]}>
                        <TouchableOpacity onPress={() => this.setState({ datePicker: !this.state.datePicker })}>
                            <Text style={[style.datePickerTxt]}>
                                {this.state.finishedAt}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TextInput
                    multiline={true}
                    style={[style.basicInput, style.descInput]}
                    value={this.state.desc}
                    onChangeText={(desc => this.setState({ desc }))}
                />
                {this.state.id != null &&
                    <>
                        <View style={style.commentsContainer}>
                            <Text style={style.commentsTitleTxt}>
                                Comments
                            </Text>
                            <TextInput
                                style={[style.basicInput, style.commentInput]}
                                placeholder="Insert new comment"
                                value={this.state.newComment}
                                onChangeText={(newComment) => this.setState({ newComment })}
                                onSubmitEditing={this.addComment}
                            />
                            <FlatList
                                style={style.commentFlatlist}
                                keyExtractor={(item, index) => index.toString()}
                                data={(this.state.comments)}
                                renderItem={(comment) => renderComment(comment.item)}
                            />
                        </View>
                        <View style={style.markAsButton}>
                            <TouchableOpacity onPress={this.markAs().onPressed}>
                                <Text style={style.markAsTitle}>
                                    {this.markAs().statusTxt()}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </>
                }
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        task: state.task
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateTask: (id, title, desc, finishedAt) => {
            dispatch(updateTask(id, title, desc, finishedAt))
        },
        createTask: (title, desc, finishedAt) => {
            dispatch(createTask(title, desc, finishedAt))
        },
        addComment: (id, comment) => {
            dispatch(addComment(id, comment))
        },
        markAs: (id, status) => {
            dispatch(markAs(id, status))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskEditor)