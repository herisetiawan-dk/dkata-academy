import { StyleSheet } from 'react-native'

const mainColor = '#009688'
const secColor = '#fff'

const style = StyleSheet.create({
    fullContainer: {
        height: '100%',
        width: '100%'
    },
    container: {
        backgroundColor: mainColor
    },
    headerContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    txtTitle: {
        color: secColor,
        fontWeight: 'bold',
        fontSize: 50
    },
    findForm: {
        backgroundColor: secColor,
        borderRadius: 20,
        paddingHorizontal: '5%',
        marginTop: '5%',
        marginBottom: 5,
        opacity: 0.75,
        width: '85%',
        height: 40,
        alignSelf: 'center'
    },

    taskListContainer: {
        alignSelf: 'center',
        width: '85%'
    },
    eachContainer: {
        backgroundColor: secColor,
        marginBottom: '5%',
        borderRadius: 10,
        padding: '3%'
    },
    titleDate: {
        flexDirection: 'row',
        justifyContent:'space-between'
    },
    taskTitleText: {
        flex: 1,
        color: mainColor,
        fontWeight: 'bold',
        fontSize: 15
    },
    taskDate: {
        color: 'grey',

    },
    taskDescText: {
        fontSize: 12
    },
    datePickerContainer: {
        flexDirection: 'row',
        width: '85%',
        alignSelf: 'center',
        marginBottom: '5%'
    },
    datePicker: {
        flex: 1,
        backgroundColor: secColor,
        opacity: 0.75,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center'
    },
    datePickerSeparator: { 
        flex: 2, 
        marginRight: 5 
    },
    datePickerTxt: {
        color: mainColor,
        textAlign: 'center',
    },
    basicInput: {
        backgroundColor: secColor,
        justifyContent: 'center',
        borderRadius: 15,
        paddingHorizontal: 15,
        color: mainColor
    },
    taskEditorContainer: {
        margin: '10%'
    },
    titleDateContainer: {
        width: '100%',
        marginBottom: 5,
        height: 50,
    },
    titleInput: {
        flex: 2,
        marginRight: 5
    },
    descInput: {
        width: '100%',
        alignSelf: 'center',
        height: '30%'
    },
    containerPadding: {
        paddingTop: '5%',
        paddingHorizontal: '5%',
        justifyContent: 'center'
    },
    commentsContainer: {
        marginTop: 5,
        backgroundColor: secColor,
        borderRadius: 15,
        padding: 15
    },
    commentsTitleTxt: {
        color: mainColor,
        fontWeight: 'bold'
    },
    commentInput: {
        color: secColor,
        backgroundColor: mainColor
    },
    commentList: {
        marginTop: 5,
        backgroundColor: mainColor,
        borderRadius: 15,
        padding: 10
    },
    commentDate: {
        color: 'lightgrey',
        textAlign: 'right',
        fontSize: 10
    },
    commentBody: {
        color: secColor,
        fontWeight: 'bold',
        
    },
    commentFlatlist: {
        height: '20%'
    },
    markAsButton: {
        marginTop: 5,
        alignSelf: 'center',
        backgroundColor: secColor,
        borderRadius: 20,
        height: 40,
        width: '80%',
        justifyContent: 'center'
    },
    markAsTitle: {
        color: mainColor,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    }
})

export { style, mainColor, secColor }