//Creating new array
arr = ['tree','leaf','wood','grass','root']

//Making function that can be reuse if needed
const counter = function (array,minLen) {
   return array.map(a => a.length).filter(a => a>=minLen).length }

//Look like our array is to short, add another string to array
arr.push('air','water','cloud','thunder','rain')

//you can use counter function, just call it with parameter
//example we only want count array with minimum length 5
counter(arr,5)

//Making another array
fruits = ['apple','banana','grape','cherry','peach','dragonfruit']

//We can use counter function to another array, ex: minLen is 3
counter(fruits,3)

//To make it simply and reused
const descArrSort = function (array) {
   return array.sort(function(a,b) {
      return b.length - a.length }
   )
}	

//Or you can use it for sorting fruits
fruits.sort(function(a,b) {return b.length - a.length})
