const { trainee, trainer } = require('./003_ThirdDay-Exc1')
const http = require('http')
const url = require('url')

var server = http.createServer((req, res) => {
    var parUrl = url.parse(req.url, true)
    var path = parUrl.pathname
    var name = path.slice(1)
    let Heri = new trainee('Heri', 'Setiawan', '19', 'men', 'anywhere', 'Coding anytime')
    let Human = new trainee('Just a', 'Human', '24', 'men', 'anywhere', 'Coding anytime')
    let Trainer = new trainer('First', 'Last', 'anytime', 'woman', 'some where i the earth', 'creating magic', 'hidden')
    let Radha = new trainer('Radha', 'Krishna', 'older than me', 'men', 'India', 'Be patient in train a trainee', 'you not allowed to see this')
    let allPeople = { heri: Heri, human: Human, trainer: Trainer, radha: Radha }
    if (allPeople[name] != undefined) {
        res.writeHead(200, { 'content-type': 'application/json' })
        res.end(JSON.stringify(allPeople[name]))
    }
})
server.listen(3000, console.log('Server is ready'))