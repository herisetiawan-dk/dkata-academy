const http = require('http')
const url = require('url')
var server = http.createServer(function (req, res) {
    if (req.method === 'GET') {
        var parUrl = url.parse(req.url, true)
        var path = parUrl.pathname
        res.writeHead(200, { 'content-type': 'application/json' })
        res.end(`Hi, i\'am ${path.slice(1)}`)

    }
})
server.listen(3000)