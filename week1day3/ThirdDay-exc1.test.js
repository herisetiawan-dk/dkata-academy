const { trainee, trainer } = require('./ThirdDay-exc1')

let thisGuy = new trainee('Heri','Setiawan','19','men','anywhere','Coding anytime')
let ourTeacher = new trainer('First','Last','anytime','woman','somewhere in the earth', 'creating magic','hidden')

test('should be greeting to this guys', () => {
  expect(thisGuy.greeting()).toBe('Hi all!! I\'m Heri Setiawan, my age is 19, i came from anywhere and i\'ve insterest to Coding anytime!!')
})

test('She should be somewhere in the earth', () => {
  expect(ourTeacher.address).toBe('somewhere in the earth')
})
