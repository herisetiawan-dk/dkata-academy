const greeting = require('./greeting')
const profile = require('./profile')
const parsetime = require('./parsetime')
const project = require('./project')
const imageDir = require('./image')

module.exports = [].concat(greeting, profile, parsetime, project, imageDir)