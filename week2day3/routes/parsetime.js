const handler = require('../handler/handlerHapi')
const regex = require('../tools/regex')

module.exports = [{
    method: 'GET',
    path: '/api/parsetime',
    handler: handler.parsetime,
    options: regex.parsetime
}]