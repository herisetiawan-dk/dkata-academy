const { trainee, trainer } = require('./ThirdDay-Exc3')

let heri = new trainee('Heri', 'Setiawan', '19', 'men', 'anywhere', 'Coding anytime')
let human = new trainee('Just a', 'Human', '24', 'men', 'anywhere', 'Coding anytime')
let radha = new trainer('Radha', 'Krishna', 'older than me', 'men', 'India', 'Be patient in train a trainee', 'you not allowed to see this')
let naveen = new trainer('Naveen', 'Ganapathy', 'older than me', 'men', 'India', 'I dont know, to much to say', 'this is privacy')
module.exports = {
    heri,
    human,
    radha,
    naveen
}