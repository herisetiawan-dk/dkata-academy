const Joi = require('@hapi/joi')

exports.username = {
    validate: {
        params: {
            name: Joi.string().regex(/^[a-zA-Z][a-z]{3,10}$/).required()
        }
    }
}

exports.parsetime = {
    validate: {
        query: {
            iso: Joi.date().iso().required()
        }
    }
}

exports.imageFile = {
    validate: {
        params: {
            // dir: Joi.string().regex(/(\.|\.(|gif|jpg|jpeg|tiff|png))$/)
        }
    }
}