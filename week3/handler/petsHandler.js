const { Pet } = require('../models/PetModel')
const path = require('path')
const fs = require('fs')

const petReqChecker = async (id) => {
    let petReq = await Pet.find({ id: id }, { _id: 0 })
    let notAvailable = !petReq[0] ? true : false
    return notAvailable
}

exports.getPet = async (filter) => {
    return await Pet.find({ id: filter }, { _id: 0 })
}

exports.upPetImg = async (request, h) => {
    let extension = path.extname(request.payload.profile_picture.hapi.filename)
    let random = Math.floor(Math.random()*10**18)
    let fileName = request.params.id +"-"+ random + extension;
    (request.payload.profile_picture).pipe(fs.createWriteStream(__dirname + "/../data/src/img/" + fileName))
    await Pet.findOneAndUpdate({ id: parseInt(request.params.id) }, { image: fileName })
    return h.response({ message: "File uploaded", file: fileName });
}

exports.pets = async (request, h) => {
    let query = request.query
    let sort = query.sort ? query.sort : 'id'
    let limit = query.limit ? query.limit : 10
    let offset = query.offset ? query.offset : 0
    let filter = query.filter ? query.filter : {}
    let result = await Pet.find(filter, { _id: 0})
        .sort(sort)
        .limit(limit)
        .skip(offset)
    return result
}

exports.petsId = async (request, h) => {
    let filter = request.params.id
    if (await petReqChecker(filter)) {
        return h.response({ message: 'Not Found' }).code(404)
    } else {
        return request.server.methods.getPet(filter)
    }
}

exports.setPet = async (request, h) => {
    let max = await Pet.aggregate([{ $group: { _id: 'max', maxId: { $max: "$id" } } }])
    let { name, breed, colour, age, next_checkup, vaccinations } = request.payload
    let nameUsePet = await Pet.find({ name: name })
    if (nameUsePet[0] != undefined) {
        return h.response({ message: 'Name already used' }).code(409)
    }
    let id = max[0]['maxId'] + 1
    let sold = false
    let addPet = new Pet({ id, name, breed, colour, age, next_checkup, vaccinations, sold })
    await addPet.save()
    h.response().code(201)
    return addPet
}

exports.putPet = async (request, h) => {
    let { params, payload } = request
    if (await petReqChecker(params.id)) {
        return h.response({ message: 'Not Found' }).code(404)
    }
    await Pet.findOneAndUpdate({ id: parseInt(params.id) }, payload)
    await request.server.methods.getPet.cache.drop(params.id)
    h.response().code(202)
    return request.server.methods.getPet(params.id)

}

exports.delPet = async (request, h) => {
    let filter = request.params.id
    if (await petReqChecker(filter)) {
        return h.response({ message: 'Not Found' }).code(404)
    } else {
        let petNotSold = await Pet.find({ id: filter, sold: false })
        if (!petNotSold[0]) {
            return h.response({ message: 'Pet is available but has been sold' }).code(404)
        }
        await Pet.findOneAndUpdate({ id: filter }, { sold: true })
        await request.server.methods.getPet.cache.drop(filter)
        h.response().code(202)
        return request.server.methods.getPet(filter)
    }
}