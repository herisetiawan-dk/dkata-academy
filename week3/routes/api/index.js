const { pets, petsId, setPet, putPet, delPet, greeting, petImage, upPetImg } = require('./pets')

module.exports = [].concat(pets, petsId, setPet, putPet, delPet, greeting, petImage, upPetImg)