PETS API
learning API with creating a petstore

# FEATURE OF THIS PETS API
Showing all pets you have
Updating your pet
Adding new pet
Deleting your pet

# REQUIREMENTS
MongoDB
Redis
Node.js

# HAPI PLUGIN
 @hapi/basic  
 @hapi/catbox-redis  
 @hapi/hapi  
 @hapi/joi@15.1.1  
 @hapi/lab  
 brcypt  
 btoa  
 mongoose  
 qs  

# BUGs ON THIS SOURCE
 still finding the bugs, if you found one, please tell me, or email to heri.setiawan@dkatalis.com

# HOW TO RUN THIS API???
 Clone this repository
 Open terminal and type 'cd dkata-academy && node start'
 And keep that running
 To get your pet, just open your browser and type 'http://localhost:3000' as a link
# HOW TO RUN THE TEST FILE

 Just open a terminal, cd to dkata-academy folder, then just type 'lab' or 'yarn lab'