const Path = require('path')
const CatboxRedis = require('@hapi/catbox-redis')
const { connectDB } = require('./mongoConnect')
const Hapi = require('@hapi/hapi')
const api = require('../routes/api')
const users = require('./users')
const Bcrypt = require('bcrypt')
const Qs = require('qs')
const { getPet } = require('../handler/petsHandler')

const server = Hapi.server({
    host: '0.0.0.0',
    port: 3000,
    query: {
        parser: (query) => Qs.parse(query)
    },
    routes: {
        files: {
            relativeTo: Path.join(__dirname, '../data/src/')
        }
    },
    cache: [
        {
            name: 'pets_cache',
            provider: {
                constructor: CatboxRedis,
                options: {
                    partition: 'pets',
                    host: '127.0.0.1',
                    port: 6379,
                    database: 0
                }
            }
        }
    ]
});

const swaggerOptions = {
    info: {
        title: 'Pets API Documentation',
        version: '0.0.1',
    }
};

const currentDate = () => {
    const curDate = new Date();
    var month = curDate.getUTCMonth() + 1;
    var day = curDate.getUTCDate();
    var year = curDate.getUTCFullYear();
    return `${day < 10 ? '0' + day : day}-${month < 10 ? '0' + month : month}-${year}`
}

const validate = async (request, username, password, h) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const register = async () => {
    await server.register([
        require('@hapi/basic'),
        require('inert'),
        require('vision'),
        {
            plugin: require('hapi-swagger'),
            options: swaggerOptions
        },
        {
            plugin: require('hapi-pino'),
            options: {
                prettyPrint: process.env.NODE_ENV !== 'production',
                stream: `./logs/${currentDate()}.log`,
                redact: ['req.headers.authorization']
            }
        }
    ]);

    server.auth.strategy('simple', 'basic', { validate });
    server.auth.default('simple');
    server.route(api);
}

server.method('getPet', getPet, {
    cache: {
        cache: 'pets_cache',
        expiresIn: 86400 * 1000,
        generateTimeout: 2000
    }
})

connectDB()

const start = async () => {
    await register()
    await server.start();
    console.log('Server running at :', server.info.uri);
    return server
};

const init = async () => {
    await server.initialize();
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

module.exports = {
    start, init, register
}